-- INSTALLATION (https://postgresql-anonymizer.readthedocs.io/en/latest/)
-- ENTER IN THE CONTAINER AS ROOT
--		git clone https://gitlab.com/dalibo/postgresql_anonymizer.git
--		sudo apt -y install make gcc postgresql-server-dev-<postgresql-version>
--		cd postgresql_anonymizer/
--		make extension PG_CONFIG=/usr/lib/postgresql/<postgresql-version>/bin/pg_config
--		sudo make install PG_CONFIG=/usr/lib/postgresql/<postgresql-version>/bin/pg_config

-- BEFORE PROCEED CHECK IF THE EXTENSION IS INSTALLED (If you get an error, the extension is probably not present on host server)
-- 		ls $(pg_config --sharedir)/extension/anon
-- 		ls $(pg_config --pkglibdir)/anon.so

ALTER DATABASE <database_name> SET session_preload_libraries = 'anon';

-- RESTART THE DATABASE: restart the instance of docker or use in terminal "sudo systemctl restart postgresql.service"

-- BEFORE PROCEED CHECK IF THE EXTENSION IS LOADED (ERROR if you don't see 'anon' in any of these parameters)
-- 		SHOW local_preload_libraries;
-- 		SHOW session_preload_libraries;
-- 		SHOW shared_preload_libraries;

CREATE EXTENSION IF NOT EXISTS anon CASCADE;

-- BEFORE PROCEED CHECK IF THE EXTENSION IS CREATED (If the result is empty, the extension is not declared in your database)
-- 		SELECT * FROM pg_extension WHERE extname= 'anon';

SELECT anon.init();

-- BEFORE PROCEED CHECK IF THE EXTENSION IS INITIALIZED (If the result is not 'true', the extension data is not present)
-- 		SELECT anon.is_initialized();

-- SQL COMMANDS TO DO THE STATIC MASKING
-- WARNING: 
--		DYNAMIC MASKING ONLY AFFECTS CERTAIN ROLES, OTHER ROLES CONTINUE TO SEE THE ORIGINAL DATA
--		STATIC MASKING REPLACES ORIGINAL DATA, OLD DATA ARE LOST FOREVER

-- USERS
SECURITY LABEL FOR anon ON COLUMN users.name
IS 'MASKED WITH FUNCTION anon.fake_first_name()';

SECURITY LABEL FOR anon ON COLUMN users.surname
IS 'MASKED WITH FUNCTION anon.fake_last_name()';

SECURITY LABEL FOR anon ON COLUMN users.email
IS 'MASKED WITH FUNCTION anon.fake_email()';

SECURITY LABEL FOR anon ON COLUMN users.city
IS 'MASKED WITH FUNCTION anon.fake_city()';

SECURITY LABEL FOR anon ON COLUMN users.phone
IS 'MASKED WITH FUNCTION anon.random_phone(phone)';

-- CUSTOMERS
SECURITY LABEL FOR anon ON COLUMN customers.name
IS 'MASKED WITH FUNCTION anon.fake_company()';

SECURITY LABEL FOR anon ON COLUMN customers.contact_name
IS 'MASKED WITH FUNCTION anon.fake_first_name()';

SECURITY LABEL FOR anon ON COLUMN customers.contact_email
IS 'MASKED WITH FUNCTION anon.fake_email()';

SECURITY LABEL FOR anon ON COLUMN customers.address
IS 'MASKED WITH FUNCTION anon.fake_address()';

-- APPLY ANONYMIZATION TO THE database
SELECT anon.anonymize_database();
